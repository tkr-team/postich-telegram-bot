module.exports = {
    // Настройки проекта
    'env': {
        // Проект для браузера
        'browser': true,
        // Включаем возможности ES6
        'es6': true,
        // Добавляем возможности ES2017
        'es2020': true
    },
    // Наборы правил
    'extends': [
        // Базовый набор правил eslint
        'eslint:recommended',
    ],
    // Движок парсинга
    'parser': 'babel-eslint',
    'parserOptions': {
        'sourceType': "module",
    },
    'plugins': [
        'eslint-plugin-node',
        'eslint-plugin-no-loops',
        'eslint-plugin-promise'
    ],
    'rules': {
        'no-loops/no-loops': 2,
        'no-var': 'error',
        'semi': 'error',
        'indent': 'error',
        'no-multi-spaces': 'error',
        'space-in-parens': 'error',
        'no-multiple-empty-lines': 'error',
        'prefer-const': 'error',
        'no-use-before-define': 'error',
        'quotes': ['error', 'single'],
        'object-curly-spacing': ['error', 'always'],
        'newline-before-return': 'error',
        'comma-dangle': ["error", {
            "arrays": "always-multiline",
            "objects": "always-multiline",
            "imports": "always-multiline",
            "exports": "always-multiline",
            "functions": "never"
        }],
        "no-console": ["error", { "allow": ["warn", "error"] }],
    }
}