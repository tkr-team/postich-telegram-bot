import path from 'path';
import { session, Telegraf } from 'telegraf';
import TelegrafI18n, { match } from 'telegraf-i18n';
import { config } from 'dotenv';
import { startScene } from './controllers/start';
import { createGroupSceneWizard } from './controllers/createGroup';
import { Scenes } from 'telegraf';
import asyncWrapper from './util/error-handler';
import { CREATE_GROUP_ID } from './controllers/createGroup/constants';
import { ADD_SOCNETWORK_ID } from './controllers/addSocNetwork/constants';
import { addSocNetworkSceneWizard } from './controllers/addSocNetwork';
import { vkPublishSceneWizard } from './controllers/vkPublish';
import { VK_PUBLISH_ID } from './controllers/vkPublish/constants';

config();
// eslint-disable-next-line no-undef
const bot = new Telegraf(process.env.TELEGRAM_TOKEN, {});
const stage = new Scenes.Stage([
    startScene,
    createGroupSceneWizard,
    addSocNetworkSceneWizard,
    vkPublishSceneWizard,
]);
const i18n = new TelegrafI18n({
    defaultLanguage: 'en',
    // eslint-disable-next-line no-undef
    directory: path.resolve(__dirname, 'locales'),
    useSession: true,
    allowMissing: false,
    sessionName: 'session',
});

bot.use(session());
bot.use(i18n.middleware());
bot.use(stage.middleware());

bot.start(asyncWrapper(async (ctx) => {
    await ctx.scene.enter('start');
}));

bot.hears(match('keyboards.main_keyboard.add_group'), asyncWrapper(async (ctx) => {
    await ctx.scene.enter(CREATE_GROUP_ID);
}));

bot.hears(match('keyboards.main_keyboard.add_socnet'), asyncWrapper(async (ctx) => {
    await ctx.scene.enter(ADD_SOCNETWORK_ID);
}));

bot.hears(match('keyboards.main_keyboard.post'), asyncWrapper(async (ctx) => {
    await ctx.scene.enter(VK_PUBLISH_ID);
}));

bot.launch();
// eslint-disable-next-line no-console
console.log('bot has been started');
