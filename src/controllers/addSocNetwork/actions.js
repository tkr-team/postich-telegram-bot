import { getGroupsLogic, getSocNetworksLogic } from './helpers';
import { getDataListPaginator } from '../../util/keyboards';
import { clamp } from '../common/helpers';
import { ELEMENTS_ON_PAGE, FIRST_PAGE_NUM, PAGE_STEP } from '../common/constants';


export const handleOnGroupPaginator = async ctx => {
    const arrow = ctx.callbackQuery.data;
    const maxPage = ctx.wizard.state.maxPage;
    let currentPage = ctx.session.page;
    if (arrow === 'right') {
        currentPage += PAGE_STEP;
        ctx.session.page = clamp(currentPage, FIRST_PAGE_NUM, maxPage);
    } else if (arrow === 'left') {
        currentPage -= PAGE_STEP;
        ctx.session.page = clamp(currentPage, FIRST_PAGE_NUM, maxPage);
    } else {
        ctx.wizard.state.chosenGroup = ctx.callbackQuery.data;
        await ctx.answerCbQuery('Группа выбрана');

        ctx.wizard.next();

        return ctx.wizard.steps[ctx.wizard.cursor](ctx);
    }

    const groupsData = await getGroupsLogic({
        tgId: ctx.from.id,
        pageNum: currentPage,
    });
    // eslint-disable-next-line prefer-const
    let { groups, count } = groupsData;

    const countOfPages = Math.ceil(count / ELEMENTS_ON_PAGE);

    return ctx.editMessageText(ctx.i18n.t('scenes.addSocNetwork.chooseGroup'),
        getDataListPaginator(
            groups,
            'group_id',
            'group_name',
            countOfPages
        ).construct(ctx.session.page).inline()
    );
};

export const handleOnChoose = async (ctx, fieldName) => {
    ctx.wizard.state[fieldName] = ctx.callbackQuery.data;

    ctx.wizard.next();

    return ctx.wizard.steps[ctx.wizard.cursor](ctx);
};

export const handleOnSocNetworksPaginator = async (ctx) => {
    const arrow = ctx.callbackQuery.data;
    const maxPage = ctx.wizard.state.maxPageSN;
    let currentPage = ctx.session.page;
    
    if (arrow === 'right') {
        currentPage += PAGE_STEP;
        ctx.session.page = clamp(currentPage, FIRST_PAGE_NUM, maxPage);
    } else if (arrow === 'left') {
        currentPage -= PAGE_STEP;
        ctx.session.page = clamp(currentPage, FIRST_PAGE_NUM, maxPage);
    } else {
        ctx.wizard.state.chosenSocNetwork = ctx.callbackQuery.data;
        await ctx.answerCbQuery(ctx.i18n.t('scenes.addSocNetwork.socNetworkChosen'));

        ctx.wizard.next();

        return ctx.wizard.steps[ctx.wizard.cursor](ctx);
    }

    const socNetworksData = await getSocNetworksLogic({
        tgId: ctx.from.id,
        pageNum: currentPage,
    });
    // eslint-disable-next-line prefer-const
    const socNetworks = socNetworksData.soc_networks || [];
    const count = socNetworksData.count;

    const countOfPages = Math.ceil(count / ELEMENTS_ON_PAGE);

    return ctx.editMessageText(ctx.i18n.t('scenes.addExistedSocNetwork.chooseSocNetwork'),
        getDataListPaginator(
            socNetworks,
            'soc_network_id',
            'name',
            countOfPages
        ).construct(ctx.session.page).inline())
        .catch(() => 42);
};