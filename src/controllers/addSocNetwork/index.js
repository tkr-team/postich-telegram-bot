import { Scenes } from 'telegraf';
import asyncWrapper from '../../util/error-handler';
import { ADD_SOCNETWORK_ID } from './constants';
import {
    addSocNetworkLogic,
    addSocNetworkToGroupLogic,
    checkIsBotHasRights,
    convertLinkToChannelName,
    getGroupsLogic,
    getSocNetworksLogic,
    addTgChannelLogic,
} from './helpers';
import { match } from 'telegraf-i18n';
import {
    getBackKeyboard,
    leaveToMain,
    getDataListPaginator,
    getSocNetNewOldChoice,
    getSocNetTypeChoice,
} from '../../util/keyboards';
import { handleOnChoose, handleOnGroupPaginator, handleOnSocNetworksPaginator } from './actions';
import { ELEMENTS_ON_PAGE } from '../common/constants';

export const addSocNetworkSceneWizard = new Scenes.WizardScene(
    ADD_SOCNETWORK_ID,
    asyncWrapper(async (ctx) => {
        ctx.session.page = 1;
        
        const { backKeyboard } = getBackKeyboard(ctx);
        await ctx.reply(ctx.i18n.t('scenes.addSocNetwork.start'), backKeyboard);

        const groupsData = await getGroupsLogic({
            tgId: ctx.from.id,
        });
        // eslint-disable-next-line prefer-const
        let { groups, count } = groupsData;

        if (!groups) {
            groups = [];
        }

        if (!groups.length) {
            await ctx.reply(ctx.i18n.t('scenes.addSocNetwork.emptyGroups'));

            return leaveToMain(ctx);
        }

        const countOfPages = Math.ceil(count / ELEMENTS_ON_PAGE);

        ctx.wizard.state.groups = groups;
        ctx.wizard.state.maxPage = countOfPages;

        await ctx.reply(ctx.i18n.t('scenes.addSocNetwork.chooseGroup'), getDataListPaginator(
            groups,
            'group_id',
            'group_name',
            countOfPages
        ).construct(ctx.session.page).inline());
    }),
    asyncWrapper(async (ctx) => {
        if (!ctx.wizard.state.chosenGroup) {
            await ctx.wizard.back();
            
            return ctx.wizard.steps[ctx.wizard.cursor](ctx);
        }

        await ctx.wizard.next();
        
        return ctx.wizard.steps[ctx.wizard.cursor](ctx);
    }),
    asyncWrapper(async (ctx) => {
        await ctx.reply(ctx.i18n.t('scenes.addSocNetwork.whatDoesSocNetworkLink'), getSocNetNewOldChoice(ctx));
    }),
    asyncWrapper(async (ctx) => {
        const chosenType = ctx.wizard.state.chosenType;

        if (chosenType === ctx.i18n.t('scenes.addSocNetwork.new')) {
            await ctx.wizard.next();
            await ctx.wizard.next();
            await ctx.wizard.next();
            
            return ctx.wizard.steps[ctx.wizard.cursor](ctx);
        } else if (chosenType === ctx.i18n.t('scenes.addSocNetwork.existed')) {
            await ctx.wizard.next();
            
            return ctx.wizard.steps[ctx.wizard.cursor](ctx);
        } else {
            await ctx.reply(ctx.i18n.t('scenes.addSocNetwork.wrongAnswer'));
            await leaveToMain(ctx);
        }
    }),
    asyncWrapper(async (ctx) => {
        ctx.wizard.state.socNetworks = [];
        ctx.session.page = 1;

        const socNetworksData = await getSocNetworksLogic({
            tgId: ctx.from.id,
        });

        const socNetworks = socNetworksData.soc_networks || [];
        const count = socNetworksData.count;

        if (!socNetworks.length) {
            await ctx.reply(ctx.i18n.t('scenes.addExistedSocNetwork.emptySocNetworks'));

            return leaveToMain(ctx);
        }

        const countOfPages = Math.ceil(count / ELEMENTS_ON_PAGE);

        ctx.wizard.state.socNetworks = socNetworks;
        ctx.wizard.state.maxPageSN = countOfPages;

        // eslint-disable-next-line no-undef
        await ctx.reply(ctx.i18n.t('scenes.addExistedSocNetwork.chooseSocNetwork'), getDataListPaginator(
            socNetworks,
            'soc_network_id',
            'name',
            countOfPages
        ).construct(ctx.session.page).inline());
    }),
    asyncWrapper(async (ctx) => {
        const groupId = ctx.wizard.state.chosenGroup;

        const socNetworkId = ctx.wizard.state.chosenSocNetwork;

        const result = await addSocNetworkToGroupLogic({
            groupId,
            socNetworkId,
        });

        if (result !== null) {
            await ctx.reply(ctx.i18n.t('scenes.addExistedSocNetwork.success'));
        } else {
            throw new Error('Error in addExistedSocNetworkWizardScene');
        }

        return leaveToMain(ctx);
    }),
    asyncWrapper(async (ctx) => {
        await ctx.reply(ctx.i18n.t('scenes.addNewSocNetwork.chooseType'), getSocNetTypeChoice(ctx));
    }),
    asyncWrapper(async (ctx) => {
        const chosenType = ctx.wizard.state.chosenSocNetworkType;
        if (chosenType === ctx.i18n.t('scenes.addNewSocNetwork.vk')) {
            await ctx.wizard.next();

            return ctx.wizard.steps[ctx.wizard.cursor](ctx);
        } else if (chosenType === ctx.i18n.t('scenes.addNewSocNetwork.tg')) {
            await ctx.wizard.next();
            await ctx.wizard.next();
            await ctx.wizard.next();
            await ctx.wizard.next();

            return ctx.wizard.steps[ctx.wizard.cursor](ctx);
        } else {
            await ctx.reply(ctx.i18n.t('scenes.addNewSocNetwork.wrongAnswer'));
            
            return leaveToMain(ctx);
        }
    }),
    asyncWrapper(async (ctx) => {
        await ctx.reply(ctx.i18n.t('scenes.addVkGroup.enterToken'));
        await ctx.wizard.next();
    }),
    asyncWrapper(async (ctx) => {
        ctx.wizard.state.token = ctx.message.text;
        await ctx.reply(ctx.i18n.t('scenes.addVkGroup.enterGroupUrl'));
        await ctx.wizard.next();
    }),
    asyncWrapper(async (ctx) => {
        const chosenGroup = ctx.wizard.state.chosenGroup;

        ctx.wizard.state.groupUrl = ctx.message.text;

        const result = await addSocNetworkLogic({
            tgId: ctx.from.id,
            type: 'vk',
            groupId: chosenGroup,
            token: ctx.wizard.state.token,
            groupUrl: ctx.wizard.state.groupUrl,
        });

        if (result !== null) {
            await ctx.reply(ctx.i18n.t('scenes.addVkGroup.success'));
        } else {
            await ctx.reply(ctx.i18n.t('scenes.addVkGroup.fail'));
        }

        return leaveToMain(ctx);
    }),
    asyncWrapper(async (ctx) => {
        await ctx.wizard.next();
        await ctx.reply(ctx.i18n.t('scenes.addTgChannel.tutorial'));

        return ctx.wizard.steps[ctx.wizard.cursor](ctx);
    }),
    asyncWrapper(async (ctx) => {
        await ctx.wizard.next();
    }),
    asyncWrapper(async (ctx) => {
        const chosenGroup = ctx.wizard.state.chosenGroup;
        const channelName = convertLinkToChannelName(ctx.message.text);
        const isBotHasRights = await checkIsBotHasRights(channelName);

        if (isBotHasRights) {
            await addTgChannelLogic({
                channelName,
                groupId: chosenGroup,
                tgId: ctx.from.id,
                url: ctx.message.text,
            });
            await ctx.reply(ctx.i18n.t('scenes.addTgChannel.success'));

            return leaveToMain(ctx);
        } else {
            await ctx.wizard.back();
            await ctx.reply(ctx.i18n.t('scenes.addTgChannel.fail'));

            return ctx.wizard.steps[ctx.wizard.cursor](ctx);
        }
    })
);

addSocNetworkSceneWizard.hears(match('keyboards.back_keyboard.back'), async(ctx) => await leaveToMain(ctx));

addSocNetworkSceneWizard.on('callback_query', async (ctx) => {
    if (ctx.callbackQuery.message.text === ctx.i18n.t('scenes.addSocNetwork.chooseGroup')) {
        await handleOnGroupPaginator(ctx);
    } else if (ctx.callbackQuery.message.text === ctx.i18n.t('scenes.addSocNetwork.whatDoesSocNetworkLink')) {
        await handleOnChoose(ctx, 'chosenType');
    } else if (ctx.callbackQuery.message.text === ctx.i18n.t('scenes.addExistedSocNetwork.chooseSocNetwork')) {
        await handleOnSocNetworksPaginator(ctx);
    } else if (ctx.callbackQuery.message.text === ctx.i18n.t('scenes.addNewSocNetwork.chooseType')) {
        await handleOnChoose(ctx, 'chosenSocNetworkType');
    }
});
