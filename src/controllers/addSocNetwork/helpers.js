import groupAPI from '../../api/groupAPI';
import userAPI from '../../api/userAPI';
import socNetworkAPI from '../../api/socNetworkAPI';
import telegramAPI from '../../api/telegramAPI';

export const getGroupsLogic = async ({ tgId, pageNum= 1, pageSize= 5 }) => {
    try {
        const userResponse = await userAPI.getUserByTgId({ tgId });

        if (!userResponse.ok) {
            throw new Error('Error in getGroupsLogic in getUserByTgId');
        }
        const userData = await userResponse.json();

        const groupsResponse = await groupAPI.getGroups({
            user_id: userData.user_id,
            pageNum,
            pageSize,
        });

        if (!groupsResponse.ok) {
            throw new Error('Error in getGroupsLogic in getGroups');
        }

        return groupsResponse.json();
    } catch (e) {
        console.error(e);
        throw new Error(e);
    }
};

export const getSocNetworksLogic = async ({ tgId, pageNum = 1, pageSize = 5 }) => {
    try {
        const userResponse = await userAPI.getUserByTgId({ tgId });

        if (!userResponse.ok) {
            throw new Error('Error in getSocNetworksLogic in getUserByTgId');
        }
        const userData = await userResponse.json();

        const socNetworks = await socNetworkAPI.getSocNetworks({
            user_id: userData.user_id,
            pageNum,
            pageSize,
        });

        if (!socNetworks.ok) {
            throw new Error('Error in getSocNetworksLogic in getSocNetworks');
        }

        return socNetworks.json();
    } catch (e) {
        console.error(e);
        throw new Error(e);
    }
};

export const addSocNetworkToGroupLogic = async ({ groupId, socNetworkId }) => {
    try {
        const result = await socNetworkAPI.addSocNetworkToGroup({
            group_id: groupId,
            socNetwork_id: socNetworkId,
        });

        if (!result.ok) {
            throw new Error('Error in addSocNetworkToGroupLogic in addSocNetworkToGroup');
        }

        return result.json();
    } catch (e) {
        console.error(e);
        throw new Error(e);
    }
};

export const addSocNetworkLogic = async ({ tgId, type, groupId, token, groupUrl }) => {
    try {
        const userResponse = await userAPI.getUserByTgId({ tgId });

        if (!userResponse.ok) {
            throw new Error('Error in getGroupsLogic in getUserByTgId');
        }

        const userData = await userResponse.json();

        const result = await socNetworkAPI.addVkSocNetwork({
            user_id: userData.user_id,
            type,
            group_id: groupId,
            token: token,
            group_url: groupUrl,
        });

        if (!result.ok) {
            throw new Error('Error in addSocNetworkLogic in addVkSocNetwork');
        }

        return result.json();
    } catch(e) {
        console.error(e);
        throw new Error(e);
    }
};

export const convertLinkToChannelName = (channelLink) => {
    const substrings = channelLink.split('/');
    const channelName = substrings[substrings.length - 1];
    
    return '@' + channelName;
};

export const checkIsBotHasRights = async (channelName) => {
    try {
        const meResponse = await telegramAPI.getMe();

        if (!meResponse.ok) {
            throw new Error('Error in checkIsBotHasRights in getMe');
        }
        const me = await meResponse.json();
        const {
            id,
        } = me.result;
        const rightsResponse = await telegramAPI.getAdmins({ chat_id: channelName });
        if (!rightsResponse.ok) {

            return false;
        } else {
            const rights = await rightsResponse.json();

            const {
                result,
            } = rights;
            // eslint-disable-next-line no-loops/no-loops
            for (const admin of result) {
                if (admin.user.id !== id) {
                    continue;
                }

                return admin.can_post_messages;
            }

            return false;
        }
    } catch(e) {
        console.error(e);
        throw new Error(e);
    }
};

export const addTgChannelLogic = async ({ channelName, groupId, tgId, url }) => {
    try {
        const userDataResponse = await userAPI.getUserByTgId({ tgId });

        if (!userDataResponse.ok) {
            throw new Error('Error in getUserByTgId in addTgChannel');
        }
        const userData = await userDataResponse.json();

        const channelInfoResponse = await telegramAPI.getInfo({ channelName });

        if (!channelInfoResponse.ok) {
            throw new Error('Error in channelInfoResponse in addTgChannel');
        }

        const channelInfo = await channelInfoResponse.json();

        const resultResponse = await socNetworkAPI.addTgSocNetwork({
            userId: userData.user_id,
            groupId,
            url: url,
            channelId: channelInfo.result.id,
        });

        if (!resultResponse.ok) {
            throw new Error('Error in addTgSocNetwork in addTgChannel');
        }

        return resultResponse.json();
    } catch(e) {
        console.error(e);
        throw new Error(e);
    }
};