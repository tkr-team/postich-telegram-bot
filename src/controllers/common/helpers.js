
export const clamp = (n, from, to) => Math.max(from, Math.min(to, n));