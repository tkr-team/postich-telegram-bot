import groupAPI from '../../api/groupAPI';
import userAPI from '../../api/userAPI';

export const createGroupLogic = async (tgId, groupName) => {
    try {
        const user = await userAPI.getUserByTgId({ tgId });

        if (!user.ok) {
            console.error('Something wrong with server');

            return Promise.resolve(null);
        }
        const userData = await user.json();

        const group = await groupAPI.addNewGroup({ user_id: userData.user_id, groupName });

        if (!group.ok) {
            console.error('Something wrong with server');

            return Promise.resolve(null);
        }

        return group.json();
    } catch(e) {
        console.error(e);
        throw new Error(e);
    }
};