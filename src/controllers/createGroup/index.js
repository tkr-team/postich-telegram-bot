import { Scenes } from 'telegraf';
import { createGroupLogic } from './helpers';
import { CREATE_GROUP_ID } from './constants';
import { getBackKeyboard, leaveToMain } from '../../util/keyboards';
import { match } from 'telegraf-i18n';
import asyncWrapper from '../../util/error-handler';

export const createGroupSceneWizard = new Scenes.WizardScene(
    CREATE_GROUP_ID, // first argument is Scene_ID, same as for BaseScene
    asyncWrapper(async (ctx) => {
        const { backKeyboard } = getBackKeyboard(ctx);
        await ctx.reply(ctx.i18n.t('scenes.createGroup.start'), backKeyboard);

        return ctx.wizard.next();
    }),
    asyncWrapper(async (ctx) => {
        const result = await createGroupLogic(ctx.from.id, ctx.message.text);
        if (result !== null) {
            await ctx.reply(ctx.i18n.t('scenes.createGroup.groupCreated'));

            await leaveToMain(ctx);
        } else {
            console.error('Error in create group logic');
            throw new Error();
        }
    })
);

createGroupSceneWizard.hears(match('keyboards.back_keyboard.back'), async(ctx) => await leaveToMain(ctx));
