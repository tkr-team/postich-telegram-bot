import { Scenes } from 'telegraf';
import asyncWrapper from '../../util/error-handler';
import { getBackKeyboard, getDataListPaginator, leaveToMain } from '../../util/keyboards';
import { match } from 'telegraf-i18n';
import { getGroupsLogic } from '../addSocNetwork/helpers';
import { ELEMENTS_ON_PAGE } from '../common/constants';
import { VK_PUBLISH_ID } from './constants';
import { handleOnGroupPaginator } from '../addSocNetwork/actions';
import { getSocNetworksByGroupIdLogic, postVideo } from './helpers';

export const vkPublishSceneWizard = new Scenes.WizardScene(
    VK_PUBLISH_ID, // first argument is Scene_ID, same as for BaseScene
    asyncWrapper(async (ctx) => {
        ctx.session.page = 1;

        const { backKeyboard } = getBackKeyboard(ctx);
        await ctx.reply(ctx.i18n.t('scenes.vkPublish.start'), backKeyboard);

        const groupsData = await getGroupsLogic({
            tgId: ctx.from.id,
        });
        // eslint-disable-next-line prefer-const
        let { groups, count } = groupsData;

        if (!groups) {
            groups = [];
        }

        if (!groups.length) {
            await ctx.reply(ctx.i18n.t('scenes.vkPublish.emptyGroups'));

            return leaveToMain(ctx);
        }

        const countOfPages = Math.ceil(count / ELEMENTS_ON_PAGE);

        ctx.wizard.state.groups = groups;
        ctx.wizard.state.maxPage = countOfPages;

        await ctx.reply(ctx.i18n.t('scenes.vkPublish.chooseGroup'), getDataListPaginator(
            groups,
            'group_id',
            'group_name',
            countOfPages
        ).construct(ctx.session.page).inline());
    }),
    asyncWrapper(async (ctx) => {
        if (!ctx.wizard.state.chosenGroup) {
            await ctx.wizard.back();

            return ctx.wizard.steps[ctx.wizard.cursor](ctx);
        }

        await ctx.wizard.next();

        return ctx.wizard.steps[ctx.wizard.cursor](ctx);
    }),
    asyncWrapper(async (ctx) => {
        const chosenGroup = ctx.wizard.state.chosenGroup;

        const socNetworksData = await getSocNetworksByGroupIdLogic({
            groupId: chosenGroup,
        });

        const socNetworks = socNetworksData.soc_networks || [];

        if (!socNetworks.length) {
            await ctx.reply(ctx.i18n.t('scenes.vkPublish.emptySocNetworks'));

            return leaveToMain(ctx);
        }
        await ctx.reply(ctx.i18n.t('scenes.vkPublish.post'));

        ctx.session.canSendPhotoAndVideo = true;

        await ctx.wizard.next();
    }),
    asyncWrapper(async (ctx) => {
        if (ctx.update.message.text) {
            ctx.reply(ctx.update.message.text);
        }

        await ctx.wizard.back();
    })
);

vkPublishSceneWizard.hears(match('keyboards.back_keyboard.back'), async(ctx) => await leaveToMain(ctx));

vkPublishSceneWizard.on('callback_query', async (ctx) => {
    if (ctx.callbackQuery.message.text === ctx.i18n.t('scenes.vkPublish.chooseGroup')) {
        await handleOnGroupPaginator(ctx);
    }
});

vkPublishSceneWizard.on('photo', async (ctx) => {
    if (ctx.session.canSendPhotoAndVideo) {
        // console.log('Photo: ');
        // console.log(ctx);
        // console.log(`Array = ${ctx.update.message.photo}`);
        const maxSize = ctx.update.message.photo.length - 1;
        // console.log(`maxSize = ${maxSize}`);
        // console.log(`element = ${ctx.update.message.photo[maxSize]}`);
        const fileUrl = await ctx.telegram.getFileLink(ctx.update.message.photo[maxSize].file_id);
        await ctx.reply(fileUrl);
    }
});

vkPublishSceneWizard.on('video', async (ctx) => {
    if (ctx.session.canSendPhotoAndVideo) {
        if (ctx.update.message.video.file_size > (10 << 20)) {
            await ctx.reply(ctx.i18n.t('scenes.vkPublish.limit'));
            
            return;
        }
        // console.log('Video: ');
        // console.log(ctx.update.message.video);
        const fileUrl = await ctx.telegram.getFileLink(ctx.update.message.video.file_id);
        // console.log(`url: ${fileUrl}`);

        await postVideo({
            groupId: ctx.wizard.state.chosenGroup,
            fileLink: fileUrl,
            file_id: ctx.update.message.video.file_id,
        });
        ctx.session.canSendPhotoAndVideo = false;
        await ctx.reply(ctx.i18n.t('scenes.vkPublish.success'));
        await leaveToMain(ctx);
    }
});
