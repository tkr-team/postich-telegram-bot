import groupAPI from '../../api/groupAPI';
import socNetworkAPI from '../../api/socNetworkAPI';


export const getSocNetworksByGroupIdLogic = async ({ groupId, pageNum= 1, pageSize= 5 }) => {
    try {
        const socNetworksResponse = await socNetworkAPI.getSocNetworksByGroupId({
            groupId,
            pageNum,
            pageSize,
        });

        if (!socNetworksResponse.ok) {
            throw new Error('Error in getSocNetworksByGroupIdLogic in getSocNetworksByGroupId');
        }

        return socNetworksResponse.json();
    } catch (e) {
        console.error(e);
        throw new Error(e);
    }
};

export const postVideo = async ({ groupId, fileLink, file_id }) => {
    try {
        const result = await groupAPI.postVideo({
            groupId,
            fileLink,
            file_id,
        });

        if (!result.ok) {
            throw new Error('Error in postVideo in postVideo');
        }

        return result.json();
    } catch(e) {
        console.error(e);
        throw new Error(e);
    }
};