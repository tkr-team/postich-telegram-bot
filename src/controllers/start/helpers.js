import userAPI from '../../api/userAPI';

export const startLogic = async ({ firstName, lastName, tgId, username }) => {
    try {
        const result = await userAPI.addNewUser({
            firstName,
            lastName,
            tgId,
            username,
        });

        if (!result.ok) {
            if (result.status === 409) {
                console.error('User already exist');
            } else {
                throw new Error('Unknown error while creating user');
            }
        }
    } catch(e) {
        console.error(e);
        throw new Error(e);
    }
};
