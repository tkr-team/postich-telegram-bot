import { Scenes } from 'telegraf';
import { startLogic } from './helpers';
import { getMainKeyboard } from '../../util/keyboards';

export const startScene = new Scenes.BaseScene('start');

startScene.enter(async (ctx) => {
    await startLogic({
        firstName: ctx.from.first_name,
        lastName: ctx.from.last_name,
        tgId: ctx.from.id,
        username: ctx.from.username,
    });

    const { mainKeyboard } = getMainKeyboard(ctx);
    await ctx.reply(ctx.i18n.t('scenes.start'), mainKeyboard);

    await ctx.scene.leave();
});
