import { Markup } from 'telegraf';
import { Key, Keyboard } from 'telegram-keyboard';

export const getMainKeyboard = (ctx) => {
    const mainKeyboardPost = ctx.i18n.t('keyboards.main_keyboard.post');
    const mainKeyboardAddGroup = ctx.i18n.t('keyboards.main_keyboard.add_group');
    const mainKeyboardAddSocnet = ctx.i18n.t('keyboards.main_keyboard.add_socnet');

    let mainKeyboard = Markup.keyboard([
        [mainKeyboardPost, mainKeyboardAddGroup, mainKeyboardAddSocnet],
    ]);
    mainKeyboard = mainKeyboard.resize();
  
    return {
        mainKeyboard,
        mainKeyboardPost,
        mainKeyboardAddGroup,
        mainKeyboardAddSocnet,
    };
};

export const leaveToMain = async (ctx) => {
    const { mainKeyboard } = getMainKeyboard(ctx);
    await ctx.reply(ctx.i18n.t('scenes.start'), mainKeyboard);
    await ctx.scene.leave();
    ctx.session.__scenes = {};
};

export const getBackKeyboard = (ctx) => {
    const backKeyboardBack = ctx.i18n.t('keyboards.back_keyboard.back');
    let backKeyboard = Markup.keyboard([backKeyboardBack]);

    backKeyboard = backKeyboard.resize();

    return {
        backKeyboard,
        backKeyboardBack,
    };
};

export function getDataListPaginator (dataArray, dataId, dataName, maxPage) {
    const minPage = 1;
    
    // eslint-disable-next-line no-unused-vars
    const dataArrayNames = dataArray.map(item => Key.callback(item[dataName], item[dataId], false));

    return Keyboard.make((page) => {
        if (maxPage > 1) {
            return Keyboard.make(dataArrayNames, { columns: 1 }).combine(
                Keyboard.make([
                    Key.callback('<----', 'left', page === minPage),
                    Key.callback('---->', 'right', page === maxPage),
                ]));
        } else {
            return Keyboard.make(dataArrayNames, { columns: 1 });
        }
    });
}

export function getSocNetTypeChoice (ctx) {
    return Keyboard.inline([
        ctx.i18n.t('scenes.addNewSocNetwork.vk'),
        ctx.i18n.t('scenes.addNewSocNetwork.tg'),
    ], { columns: 1 });
}

export function getSocNetNewOldChoice (ctx) {
    return Keyboard.inline([
        ctx.i18n.t('scenes.addSocNetwork.new'),
        ctx.i18n.t('scenes.addSocNetwork.existed'),
    ]);
}