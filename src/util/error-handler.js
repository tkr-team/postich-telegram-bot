import logger from './logger';
import { getMainKeyboard } from './keyboards';

/**
 * Wrapper to catch async errors within a stage. Helps to avoid try catch blocks in there
 * @param fn - function to enter a stage
 */
const asyncWrapper = (fn) => {
    // eslint-disable-next-line no-unused-vars
    return async function (ctx, next) {
        try {
            return await fn(ctx);
        } catch (error) {
            logger.error(ctx, 'asyncWrapper error, %O', error);
            
            const { mainKeyboard } = getMainKeyboard(ctx);
            await ctx.scene.leave();
            await ctx.reply(ctx.i18n.t('shared.something_went_wrong'), mainKeyboard);
            
            // return next();
        }
    };
};

export default asyncWrapper;
