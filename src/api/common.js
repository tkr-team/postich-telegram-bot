import { config } from 'dotenv';
import fetch from 'node-fetch';
config();

// eslint-disable-next-line no-undef
const baseUrl = process.env.BE_URL;

export const instance = {
    get(url) {
        return fetch(baseUrl + url);
    },
    post(url, data) {
        return fetch(baseUrl + url, {
            method: 'POST',
            body: JSON.stringify(data),
        });
    },
};