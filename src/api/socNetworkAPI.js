import { instance } from './common';

export default {
    getSocNetworks({ user_id, pageNum, pageSize }) {
        return instance.get(
            `socNetworks/byUser?userId=${user_id}&pageNum=${pageNum}&pageSize=${pageSize}`
        );
    },
    addSocNetworkToGroup({ group_id, socNetwork_id }) {
        return instance.post(
            'socNetworksToGroup',
            {
                group_id,
                socNetwork_id,
            }
        );
    },
    addVkSocNetwork({ user_id, group_id, token, group_url }) {
        return instance.post(
            'socNetworks/VK',
            {
                user_id,
                group_id,
                vkInfo_token: token,
                vkPage_url: group_url,
            }
        );
    },
    addTgSocNetwork({ userId, groupId, url, channelId }) {
        return instance.post(
            'socNetworks/TG',
            {
                user_id: userId,
                group_id: groupId,
                url: url,
                channel_id: channelId,
            }
        );
    },
    getSocNetworksByGroupId({ groupId, pageNum = 1, pageSize = 5 }) {
        return instance.get(
            `socNetworks/byGroup?groupId=${groupId}&pageNum=${pageNum}&pageSize=${pageSize}`
        );
    },
};