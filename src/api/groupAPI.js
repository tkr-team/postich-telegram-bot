import { instance } from './common';

export default {
    addNewGroup({ user_id, groupName }) {
        return instance.post(
            'groups',
            {
                'group_name': groupName,
                'user_id': user_id,
            }
        );
    },
    getGroups({ user_id, pageNum = 1, pageSize = 5 }) {
        return instance.get(
            `groups?userId=${user_id}&pageNum=${pageNum}&pageSize=${pageSize}`
        );
    },
    postVideo({ groupId, fileLink, file_id }) {
        return instance.post(
            'groups/post',
            {
                group_id: groupId,
                url: fileLink,
                file_id,
            }
        );
    },
};