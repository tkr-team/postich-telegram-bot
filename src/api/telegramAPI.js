import fetch from 'node-fetch';
import { config } from 'dotenv';

config();
// eslint-disable-next-line no-undef
const telegramLink = `https://api.telegram.org/bot${process.env.TELEGRAM_TOKEN}/`;

export default {
    getMe() {
        return fetch(telegramLink + 'getMe', {
            method: 'POST',
        });
    },
    getAdmins({ chat_id }) {
        const body = JSON.stringify({
            chat_id,
        });

        return fetch(telegramLink + 'getChatAdministrators', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': body.length,
            },
            body: body,
        });
    },
    getInfo({ channelName }) {
        const body = JSON.stringify({
            chat_id: channelName,
        });

        return fetch(telegramLink + 'getChat', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': body.length,
            },
            body: body,
        });
    },
};