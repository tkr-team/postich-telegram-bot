import { instance } from './common';

export default {
    addNewUser({ firstName, lastName, tgId, username }) {
        return instance.post(
            'users',
            {
                'user_firstName': firstName,
                'user_lastName': lastName,
                'user_tgId': tgId,
                'user_username': username,
            }
        );
    },
    getUserByTgId({ tgId }) {
        return instance.get(
            `users?tgId=${tgId}`
        );
    },
};

